const axios = require('axios')
const moment = require('moment')
const getCurrentWeather = async(req, res)=>{
    const dayTemp = []
    const minTemp = []
    const maxTemp = []
    const labels = []
    try {
        location = req.location
        const response = await axios.get(`https://api.openweathermap.org/data/2.5/onecall?lat=${location.latitude}&lon=${location.longitude}&units=metric&exclude=current,minutely,hourly&appid=481ec75a088b5094533f2cbc54417a53`)
        response.data.daily.forEach(day=>{
            labels.push(moment.unix(day.dt).format("dddd, MMMM Do YYYY"))
            dayTemp.push(day.temp.day)
            minTemp.push(day.temp.min)
            maxTemp.push(day.temp.max)

        })
        // return res.status(200).json(response.data)
        return res.status(200).render('chart', {day: dayTemp, min: minTemp, max: maxTemp, labels: labels})
    } catch (err) {
        return res.status(500).json(err)
    }
}

module.exports.getCurrentWeather = getCurrentWeather