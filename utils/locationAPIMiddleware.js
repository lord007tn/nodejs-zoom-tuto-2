const axios = require('axios')

module.exports = async (req, res, next) => {

    if(!req.location){

        try {
            const location = await axios.get('http://api.ipstack.com/check?access_key=a95b383d9c48fb89dc5e314973fc8c51')
            req.location = location.data
            console.log(req.location)
            next()
        } catch (err) {
            return res.status(500).json(err)
        }
    }
}