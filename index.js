const path = require('path')
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
//View engine

app.set('views',path.join(__dirname,'views'))
app.set('view engine', 'ejs')

//Import Routes

const weatherRoutes = require('./routes/weather.routes')

//Middleware

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

//Middleware Routes
app.use('/weather', weatherRoutes)

//server listen

const port = 8000
app.listen(port, ()=>{
    console.log('server yemchi jawou behi ' + port)
})