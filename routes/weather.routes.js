const router = require('express').Router()
const weatherControllers = require('../controllers/weatherAPI.controllers')
const locationMiddleware = require('../utils/locationAPIMiddleware')

router.get('/', locationMiddleware, weatherControllers.getCurrentWeather)

module.exports = router